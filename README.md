# OpenPowerlifting server Docker image

Gitlab CI pipeline to create Docker images based on the [opl-data Dockerfile](https://gitlab.com/openpowerlifting/opl-data/-/blob/main/Dockerfile).
